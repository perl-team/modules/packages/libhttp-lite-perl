libhttp-lite-perl (2.44-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libhttp-lite-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 15:51:21 +0100

libhttp-lite-perl (2.44-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jotam Jr. Trejo from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Jun 2022 23:04:26 +0100

libhttp-lite-perl (2.44-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 00:47:40 +0100

libhttp-lite-perl (2.44-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/copyright: drop stanza about removed files.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Jul 2015 22:21:50 +0200

libhttp-lite-perl (2.43-2) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Mark package as autopkgtest-able
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libcgi-pm-perl

 -- Damyan Ivanov <dmn@debian.org>  Fri, 12 Jun 2015 20:42:39 +0000

libhttp-lite-perl (2.43-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 17 Feb 2014 21:10:11 +0100

libhttp-lite-perl (2.42-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * debian/watch: add uversionmangle in case upstream goes back to
    single-digit minor versions.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Tue, 22 Oct 2013 19:29:30 +0200

libhttp-lite-perl (2.4-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Nuno Carvalho ]
  * d/control: Update Standards-Version to 3.9.3.
  * d/copyright: Update format and field names.
  * New upstream release.

 -- Nuno Carvalho <smash@cpan.org>  Tue, 07 Aug 2012 20:56:38 +0100

libhttp-lite-perl (2.3-1) unstable; urgency=low

  * Initial Release (closes: Bug#625800).

 -- Jotam Jr. Trejo <jotamjr@debian.org.sv>  Fri, 06 May 2011 22:59:32 -0600
